# Kobo Personalize 

## 1) Change the images:

# Logo: http://kf.cyberzen.ec/static/compiled/kobologo.svg
# Logo: http://kf.cyberzen.ec/static/img/kobologo.svg
# Fondo de la pantalla de ingreso: http://kf.cyberzen.ec/static/compiled/signup_photo.jpg
# Favicon: https://kf.cyberzen.ec/static/img/favicon.ico

# kobologo.svg
cp img_dest/kobologo.svg /opt/kobo-docker/.vols/static/kobocat/images/kobologo.svg
cp img_dest/kobologo.svg /opt/kobo-docker/.vols/static/kpi/compiled/kobologo.svg
cp img_dest/kobologo.svg /opt/kobo-docker/.vols/static/kpi/img/kobologo.svg
cp img_dest/kobologo.svg /opt/kobo-docker/.vols/static/kpi/kobologo.svg
cp img_dest/kobologo.svg /opt/kobo-docker/nginx/maintenance/www/kobologo.svg
cp img_dest/kobologoS.svg /opt/kobo-docker/.vols/static/kpi/img/kobologoS.svg
cp img_dest/kobologoS.svg /opt/kobo-docker/.vols/static/kpi/compiled/kobologoS.svg

# signup_photo.jpg
cp img_dest/signup_photo.jpg /opt/kobo-docker/.vols/static/kpi/compiled/signup_photo.jpg
cp img_dest/signup_photo.jpg /opt/kobo-docker/.vols/static/kpi/img/signup_photo.jpg

# favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kobocat/images/favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kpi/favicon-16x16.png
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kpi/favicon-32x32.png
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kpi/favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kpi/img/favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/.vols/static/kpi/rest_framework/docs/img/favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/enketo_express/favicon.ico
cp img_dest/favicon.ico /opt/kobo-docker/nginx/maintenance/www/favicon.ico
cp img_dest/favicon.ico /usr/share/gitweb/static/git-favicon.png

# apple-touch-icon.png

cp img_dest/apple-touch-icon-114x114.png /opt/kobo-docker/.vols/static/kobocat/images/apple-touch-icon-114x114.png
cp img_dest/apple-touch-icon-72x72.png /opt/kobo-docker/.vols/static/kobocat/images/apple-touch-icon-72x72.png
cp img_dest/apple-touch-icon.png /opt/kobo-docker/.vols/static/kobocat/images/apple-touch-icon-precomposed.png
cp img_dest/apple-touch-icon.png /opt/kobo-docker/.vols/static/kobocat/images/apple-touch-icon.png
cp img_dest/apple-touch-icon.png /opt/kobo-docker/.vols/static/kpi/apple-touch-icon.png

# hide k-drawer__secondary-icons bar

#sed -i 's/k-drawer__secondary-icons {\\n  left: 0;\\n  bottom: 0;\\n  position: absolute;\\n  width: 58px; }/k-drawer__secondary-icons {\\n  display:none;\\n  left: 0;\\n  bottom: 0;\\n  position: absolute;\\n  width: 58px; }/g' /opt/kobo-docker/.vols/static/kpi/compiled/app-cc4c06a80438d4c10778.js
